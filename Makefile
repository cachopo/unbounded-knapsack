CFLAGS = -O3 -Wall -pedantic

UnboundedKnapsack: UnboundedKnapsack.o

clean:
	$(RM) UnboundedKnapsack UnboundedKnapsack.o
