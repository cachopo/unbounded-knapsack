#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_SIZE 10000

#define P_AMOUNT 0 /* posição da quantidade na struct */
#define P_BEST_AMOUNT 1 /* posição da melhor quantidade na struct */
#define P_VALUE 2 /* posição do valor na struct */
#define P_WEIGHT 3 /* posição do peso na struct */

typedef struct sItem Item, *pItem;

struct sItem {
  int amount; /* quantidade */
  int best_amount; /* melhor quantidade */
  int value; /* valor */
  int weight; /* peso */
};

typedef struct sKnapsack Knapsack, *pKnapsack;

struct sKnapsack {
  int items_number; /* número de itens */
  int weight_limit; /* peso limite */
  Item items[BUFFER_SIZE]; /* itens */
};

short getFile(pKnapsack pK, char *filename);

void bubbleSort(pKnapsack pK);

void branchAndBound(pKnapsack pK);

int dotProduct(pKnapsack pK, int p1, int p2, int index);

short putFile(pKnapsack pK);

int main(int argc, char *argv[]) {
  char filename[BUFSIZ];
  Knapsack K = {0};

  if (argv[1] == NULL) {
    printf("Digite o nome do arquivo de entrada (e.g. input.txt): ");
    if (scanf("%[^\n]s", filename) != 1) {
      perror(NULL);
      exit(EXIT_FAILURE);
    }
  } else {
    strcpy(filename, argv[1]);
  }

  if (!getFile(&K, filename)) {
    perror(NULL);
    exit(EXIT_FAILURE);
  }

  bubbleSort(&K);
  branchAndBound(&K);

  if (!putFile(&K)) {
    perror(NULL);
    exit(EXIT_FAILURE);
  }

  return EXIT_SUCCESS;
}

short getFile(pKnapsack pK, char *filename) {
  FILE *pFile;
  char string[BUFFER_SIZE];
  int i = 0;
  char *pString;
  int j;

  if ((pFile = fopen(filename, "r")) == NULL) {
    return 0;
  }

  while (fgets(string, BUFFER_SIZE, pFile) != NULL) {
    switch (i++) {
    case 0:
      pString = strtok(string, " ");
      pK->items_number = atoi(pString);
      break;
    case 1:
      pString = strtok(string, " ");
      pK->weight_limit = atoi(pString);
      break;
    case 2:
      for (j = 0, pString = strtok(string, " "); j < pK->items_number && pString != NULL; j++, pString = strtok(NULL, " ")) {
        pK->items[j].value = atoi(pString);
      }
      break;
    case 3:
      for (j = 0, pString = strtok(string, " "); j < pK->items_number && pString != NULL; j++, pString = strtok(NULL, " ")) {
        pK->items[j].weight = atoi(pString);
      }
      break;
    }
  }

  fclose(pFile);

  return 1;
}

void bubbleSort(pKnapsack pK) {
  int swapped;
  int i;
  int j;
  Item aux;

  do {
    swapped = 0;
    for (i = 0, j = i + 1; i < pK->items_number - 1; i++, j++) {
      /* se a razão entre a utilidade e o peso do item n é menor que a do item n + 1... */
      if ((float) pK->items[i].value / pK->items[i].weight < (float) pK->items[j].value / pK->items[j].weight) {
        /* ...então os itens são trocados */
        aux = pK->items[i];
        pK->items[i] = pK->items[j];
        pK->items[j] = aux;
        swapped = 1;
      }
    }
  } while (swapped == 1);
}

void branchAndBound(pKnapsack pK) {
  int i;
  int remaining_weight;
  int pK_best_value;
  int remaining_weight_value;
  int estimation;
  int j;
  int candidate;

  /* cria o primeiro ramo */
  for (i = 0; i < pK->items_number; i++) {
    /* calcula o peso restante */
    remaining_weight = pK->weight_limit - dotProduct(pK, P_AMOUNT, P_WEIGHT, i);
    /* calcula a maior quantidade possível para o item atual */
    pK->items[i].best_amount = pK->items[i].amount = (int) remaining_weight / pK->items[i].weight;
  }
  /* por enquanto, o primeiro ramo corresponde à melhor solução */
  pK_best_value = dotProduct(pK, P_BEST_AMOUNT, P_VALUE, pK->items_number);
  while (1) {
    i = pK->items_number - 1;
    /* encontra o último item com quantidade diferente de 0 */
    while (i >= 0 && pK->items[i].amount == 0) {
      i--;
    }
    /* verifica se é o item encontrado na iteração anterior é o menos significativo */
    if (i == pK->items_number - 1) {
      pK->items[i].amount = 0;
      continue;
    }
    if (i >= 0) {
      /* subtrai 1 da quantidade do item encontrado na iteração anterior */
      pK->items[i].amount--;
      /* calcula o valor do peso restante para o próximo item */
      remaining_weight_value = pK->items[i + 1].value * (pK->weight_limit - dotProduct(pK, P_AMOUNT, P_WEIGHT, i + 1));
      /* estima a solução para o valor do peso restante */
      estimation = dotProduct(pK, P_AMOUNT, P_VALUE, i + 1) + (remaining_weight_value / pK->items[i + 1].weight);
      /* se a estimativa é menor ou igual à melhor solução, o ramo é podado */
      if (estimation <= pK_best_value) {
        continue;
      } else {
        /* cria um ramo com as novas quantidades a partir do item atual */
        for (j = ++i; j < pK->items_number; j++) {
          remaining_weight = pK->weight_limit - dotProduct(pK, P_AMOUNT, P_WEIGHT, j);
          pK->items[j].amount = (int) remaining_weight / pK->items[j].weight;
        }
        /* calcula a candidata a melhor solução */
        candidate = dotProduct(pK, P_AMOUNT, P_VALUE, pK->items_number);
        /* se a candidata a melhor solução é maior que a melhor solução... */
        if (candidate > pK_best_value) {
          for (i = 0; i < pK->items_number; i++) {
            pK->items[i].best_amount = pK->items[i].amount;
          }
          /* ...então a candidata a melhor solução é a melhor solução */
          pK_best_value = candidate;
        }
      }
    } else {
      break;
    }
  }
}

int dotProduct(pKnapsack pK, int p1, int p2, int index) {
  int i;
  int dotProduct = 0;

  for (i = 0; i < index; i++) {
    /* a aritmética de ponteiros depende dos valores passados como parâmetro para p1 e p2 */
    /* *(((int *) &pM->itens[i]) + 0) = pM->itens[i].quantidade, por exemplo */
    dotProduct += *(((int *) &pK->items[i]) + p1) * *(((int *) &pK->items[i]) + p2);
  }

  return dotProduct;
}

short putFile(pKnapsack pK) {
  FILE *pFile;
  int i;

  if ((pFile = fopen("output.txt", "w")) == NULL) {
    return 0;
  }

  fprintf(pFile, "Itens usados:\n");
  fprintf(pFile, "+------------+------------+------------+------------+\n");
  fprintf(pFile, "| Item*      | Quantidade | Utilidade  | Peso       |\n");
  fprintf(pFile, "+------------+------------+------------+------------+\n");
  for (i = 0; i < pK->items_number; i++) {
    if (pK->items[i].best_amount > 0) {
      fprintf(pFile, "| % 10d | % 10d | % 10d | % 10d |\n", i + 1, pK->items[i].best_amount, pK->items[i].value, pK->items[i].weight);
    }
  }
  fprintf(pFile, "+------------+------------+------------+------------+\n\n");
  fprintf(pFile, "Utilidade total: %d\n", dotProduct(pK, P_BEST_AMOUNT, P_VALUE, pK->items_number));
  fprintf(pFile, "Peso usado/peso limite: %d/%d\n", dotProduct(pK, P_BEST_AMOUNT, P_WEIGHT, pK->items_number), pK->weight_limit);

  fclose(pFile);

  return 1;
}
